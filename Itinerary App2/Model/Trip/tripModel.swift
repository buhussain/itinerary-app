//
//  tripModel.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/4/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import Foundation
import UIKit

class TripModel {
    let id: UUID  // ! means its required.
    var title: String  // ! means its required.
    var image: UIImage? // to hold the image of the trip, ? because it can be nil, so its optional.

    init (title: String, image: UIImage? = nil){
        
        // UUID class used to create a uniqe identifier
        id = UUID()
        self.title = title
        self.image = image
    }
}

