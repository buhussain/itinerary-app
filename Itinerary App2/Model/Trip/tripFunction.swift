//
//  tripFunction.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/4/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import Foundation
import UIKit
class TripFuctions {
    
    //static allaws you to access the function with out initialzing the class each time.
    
    static func createTrip(tripModel: TripModel){
        Data.tripModels.append(tripModel)
    }
    
    // this is a completion handeler that will get called when the fucntion is finished
    static func readTrip(completion: @escaping () -> ()){
        
        // DispatchQueue : Manage what work occures on what threads
        // Quality of Service (qos) : telling the application the priority
        // async : your app won't wait for your code to finish. it'll continue on.
        DispatchQueue.global(qos: .userInteractive).async {
            
            //check if there is trip models or not.
            if Data.tripModels.count == 0 {
                
                //populate the data.
                Data.tripModels.append(TripModel(title: "Kuwait"))
                Data.tripModels.append(TripModel(title: "Dubia"))
                Data.tripModels.append(TripModel(title: "Germany"))
                
            }
        }
        
        // function is finish and has some data
        DispatchQueue.main.async {
            completion()
        }
        
        
    }
    
    static func updateTrip(at index: Int, title: String, image: UIImage? = nil){
        Data.tripModels[index].title = title
        Data.tripModels[index].image = image
        
    }
    
    //i just need to pass in the trip id not the whole model.
    static func deleteTrip(index: Int){
        Data.tripModels.remove(at: index)
    }
}
