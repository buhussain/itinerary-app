//
//  ButtonExtenstion.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/6/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

extension UIButton {

    func createFloatingActionButton(){
   
    
    backgroundColor = Theme.tint
        
        
    //to make the button circle (addButton.frame.height / 2
    layer.cornerRadius = frame.height / 2
    layer.shadowOpacity = 0.25
    layer.shadowRadius = 5
    layer.shadowOffset = CGSize(width: 0, height: 10)
    }
    
}
