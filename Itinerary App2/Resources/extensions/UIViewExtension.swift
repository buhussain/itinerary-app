//
//  UIViewExtension.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/5/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

extension UIView {

    func addShadowAndRoundedCorner(){
        
        layer.shadowOpacity = 1    // drop shadow for the trip tabel cell
        layer.shadowOffset = CGSize.zero // position of shadow
        layer.shadowColor = UIColor.darkGray.cgColor // color of shadow
        layer.cornerRadius = 10 // rounded corner cells
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
