//
//  appUIButton.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/6/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

class appUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
         backgroundColor = Theme.tint

        layer.cornerRadius = frame.height / 2
        
        setTitleColor(UIColor.white, for: .normal)
      
        
    
    }
    


}
