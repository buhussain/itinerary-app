//
//  PopupUIView.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/6/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

class PopupUIView: UIView {

    // i used init and not draw, because this control need to be inilized once only.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.shadowOpacity = 1    // drop shadow for the trip tabel cell
        layer.shadowOffset = CGSize.zero // position of shadow
        layer.shadowColor = UIColor.darkGray.cgColor // color of shadow
        layer.cornerRadius = 10 // rounded corner cells
        
        backgroundColor = Theme.Background
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
