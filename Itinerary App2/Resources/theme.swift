//
//  theme.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/5/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

class Theme {
    
    static let mainFontName = "SimSun"

    static let accent = UIColor(named: "Accent")
    static let Background = UIColor(named: "Background")
    static let tint = UIColor(named: "Tint")
    
}
