//
//  TripsTabelViewCell.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/5/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

class TripsTabelViewCell: UITableViewCell {

    @IBOutlet var cardView: UIView!
    
    @IBOutlet var tripTitle: UILabel!
    
    @IBOutlet weak var tripImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code, this code will hapen first when the cell is created.

        // function from the UIViewExtension.
        cardView.addShadowAndRoundedCorner()
        
        tripTitle.font = UIFont(name: Theme.mainFontName, size: 32)
        cardView.backgroundColor = Theme.accent
        tripImageView.layer.cornerRadius = cardView.layer.cornerRadius

        
    }
    
    //populate the data in the cells.
    
    func setup(tripModel: TripModel){
        tripTitle.text = tripModel.title
        tripImageView.image = tripModel.image
    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
