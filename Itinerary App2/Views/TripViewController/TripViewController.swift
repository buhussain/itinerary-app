//
//  TripViewController.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/5/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit

class TripViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var tripIndexToEdit: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        TripFuctions.readTrip(completion: { [weak self] in
            
            // this code it called when the complation handeler get called.
            self?.tableView.reloadData()
            
            
            })
        //view.backgroundColor = Theme.Background
        view.backgroundColor = Theme.Background
       
        //addButton.backgroundColor = Theme.tint
        
        //to make the button circle (addButton.frame.height / 2
        //addButton.layer.cornerRadius = addButton.frame.height / 2
        //addButton.layer.shadowOpacity = 0.25
        //addButton.layer.shadowRadius = 5
        //addButton.layer.shadowOffset = CGSize(width: 0, height: 10)
        
        // this function from ButtonExtenstion.
        addButton.createFloatingActionButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toAddTripSegue" {
            let popup = segue.destination as! AddTripViewController
            popup.tripIndexToEdit = self.tripIndexToEdit
            popup.doneSaving = { [weak self] in
                
            //inline function.
            self?.tableView.reloadData()
                
            }
        }
    }
}

extension TripViewController: UITableViewDataSource, UITableViewDelegate {
    
    // papulate a table view you need , source , how many rowns and what to show.
    // source : tableView.dataSource = self
    // how many rows : Data.tripModels.count
    // what to show: cell.textLabel?.text = Data.tripModels[indexPath.row].title
    
    // get the number or rowns
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // how many rows in the table.
        return Data.tripModels.count
    }
    
    // what to show.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // when a cell get off the screen, dequeue it. for better
        // memory manegment.
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TripsTabelViewCell
        
        // i used (as TripsTabelViewCell) because dequeueReusableCell return a reguler cell where in
        // my code i used a custom cell
        
        // this was commented because i have prototype cell.
        // if there is no cells
        // if cell == nil {
        //
        //    cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        //
        //}
        
        //indexPath to access the table row.
        // title is the item i want to show in the table view.
        
        // i can remove this function and call the setup function.
        //cell!.textLabel?.text = Data.tripModels[indexPath.row].title
        
        cell.setup(tripModel: Data.tripModels[indexPath.row])
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // create action (delete), then add it to the UISwipeActionsConfiguration
        
        let delete = UIContextualAction(style: .destructive, title: "delete") {
            (UIContextualAction, view, actionPerformed: @escaping (Bool)->Void) in
            
            //present alert action.
            let alert = UIAlertController(title: "Delete Trip", message: "are you sure", preferredStyle: .alert)
            
            //handel cancel action for the alert view
            alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (alertAction) in
                actionPerformed(false)
            }))
            
            //handel the delete action for the alert view
            alert.addAction(UIAlertAction(title: "delete", style: .destructive, handler: { (alertAction) in
                //preform the delete.
                TripFuctions.deleteTrip(index: indexPath.row)
                
                //reload the data. but no animation
                //tableView.reloadData()
                
                //delete the row with some animation.
                tableView.deleteRows(at: [indexPath], with: .automatic)
                actionPerformed(true)
            }))
            
            //present the alert control.
            self.present(alert, animated: true)
            
        }
        
        delete.image = UIImage(named: "delete")
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let edit = UIContextualAction(style: .normal, title: "edit") { (contextualAction, view, actionPerformed: (Bool) -> ()) in
            
            self.tripIndexToEdit = indexPath.row
            
            //what happen if the user click the edit button
            self.performSegue(withIdentifier: "toAddTripSegue", sender: nil)
            actionPerformed(true)
            
        }
        
        return UISwipeActionsConfiguration(actions: [edit])
    }
    
    
    
    
}
