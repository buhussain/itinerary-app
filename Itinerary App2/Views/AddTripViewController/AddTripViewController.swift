//
//  AddTripViewController.swift
//  Itinerary App2
//
//  Created by Waleed M. Al-Attar on 10/6/18.
//  Copyright © 2018 Waleed mohammad Al-Attar. All rights reserved.
//

import UIKit
import Photos

extension AddTripViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){

        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imageView.image = image
        }

        dismiss(animated: true)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    
}

class AddTripViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tripTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
  
    
    //call back functions.
    var doneSaving: (() -> ())?
    var tripIndexToEdit: Int?

    func presentPickerControl()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .photoLibrary
        self.present(myPickerController, animated: true)
    }
    
    @IBAction func addPhoto(_ sender: UIButton) {
        
        //check if the photo library excists
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            //check if you have access to the photo library
            PHPhotoLibrary.requestAuthorization {(status) in
               
                //status: if the user select yes or no.
                switch status {
                    
                case .authorized:
                    self.presentPickerControl()

                case .notDetermined:
                    if status == PHAuthorizationStatus.authorized {
                      self.presentPickerControl()
                    }
                
                case .restricted:
                    let alert = UIAlertController(title: "alert title", message: "the message", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: .default)
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true)
                
                case .denied:
                    let alert = UIAlertController(title: "access denied", message: "the message", preferredStyle: .alert)
                    
                    let gotoSetting = UIAlertAction(title: "go to setting", style: .default) {(action) in
                        DispatchQueue.main.async {
                            let url = URL(string: UIApplication.openSettingsURLString)!
                            UIApplication.shared.open(url, options: [:])
                        }
                    }
                    let cancelAction = UIAlertAction(title: "cancel", style: .cancel)
                    alert.addAction(gotoSetting)
                    alert.addAction(cancelAction)
                    self.present(alert, animated: true)
                }
                
                
            }
            
        }
        
    }
    
    
    
    
    
    @IBAction func cancel(_ sender: UIButton) {
        
        //dismiss(animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
        // completion : fuction get called when the didmiss action is complated, notes (?) this means its optional so you can just remove (()->void
        
        dismiss(animated: true)
    }
    

    
    @IBAction func save(_ sender: UIButton) {
        
        tripTextField.rightViewMode = .never
        
        // the guard statmetn is for the exception
        guard tripTextField.text != "" , let newTripName = tripTextField.text else {
            
            // HOW TO ADD AN IMAGE TO A TEXT VIEW.
            // if guard tripTextField.text != "" is true,
            // then let newTripName = tripTextField.text
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
            imageView.image = UIImage(named: "warning")
            imageView.contentMode = .scaleAspectFit

            tripTextField.rightView = imageView
 
            tripTextField.rightViewMode = .always
            return
            
        }
        
        if let index = tripIndexToEdit {
            TripFuctions.updateTrip(at: index, title: newTripName, image: imageView.image)
        } else {
           TripFuctions.createTrip(tripModel: TripModel(title: newTripName, image: imageView.image))
        }
        
        
        
        
        // i used if let beacuse doneSaving might return nil
        if let doneSaving = doneSaving {
            
            doneSaving()
            
        }
        
        //dismiss(animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
        // completion : fuction get called when the didmiss action is complated, notes (?) this means its optional so you can just remove (()->void
        
        dismiss(animated: true)
    }
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 24)
        imageView.layer.cornerRadius = 10
        
        //drop shadow on the title.
        titleLabel.layer.shadowOpacity = 1
        titleLabel.layer.shadowColor = UIColor.white.cgColor
        titleLabel.layer.shadowOffset = CGSize.zero
        titleLabel.layer.shadowRadius = 5
        
        if let index = tripIndexToEdit {
            let trip = Data.tripModels[index]
            tripTextField.text = trip.title
            imageView.image = trip.image
        }
        
        
        // Do any additional setup after loading the view.
    }
    


}
